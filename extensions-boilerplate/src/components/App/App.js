import React from 'react'
import Authentication from '../../util/Authentication/Authentication'
import { Container, Button, Segment } from 'semantic-ui-react'
import axios from 'axios'

import Countdown from 'react-countdown-now';
import './Button.css'
import './App.css'
import cardDetails from './set1-en_us.json'
import socketIOClient from 'socket.io-client';
import sailsIOClient from 'sails.io.js';

export default class App extends React.Component{
    constructor(props){
        super(props)
        this.Authentication = new Authentication()
        this.SERVER_ADDRESS = 'https://1b505761.ngrok.io';
        this.BACKEND_URL = 'http://localhost:1337';
        this.TIME_TO_PREDICT = 15000;
        //if the extension is running on twitch or dev rig, set the shorthand here. otherwise, set to null. 
        this.twitch = window.Twitch ? window.Twitch.ext : null
        this.currentPlay = null;
        this.state= {
            finishedLoading:false,
            theme:'light',
            isVisible:true,
            cardsInHand: [],
            predictionActive: false,
            status: "INITIAL",
            prediction: []
        }
    }

    contextUpdate(context, delta){
        if(delta.includes('theme')){
            this.setState(()=>{
                return {theme:context.theme}
            })
        }
    }

    visibilityChanged(isVisible){
        this.setState(()=>{
            return {
                isVisible
            }
        })
    }

    startPred() {
        this.getCardIds().then((res) => {
            // console.log(res.data)
            this.io.socket.post(`/streamer/start/`, {
                hand: res.data
            },
            (body, JWR) => {
                // body === JWR.body
                console.log('Sails responded with: ', body);
                this.currentPlay = body.id;
                const cardsInHand = body.cards;

                console.log('with headers: ', JWR.headers);
                console.log('and with status code: ', JWR.statusCode);
                this.setState({
                    cardsInHand,
                    predictionActive: true,
                    timerEnded: false,
                    status: "TIMER",
                })
            })

        })
 
    }

    stopPred() {
        this.io.socket.post(`/streamer/stop/`, {
            hand: res.data
        },
        (body, JWR) => {
            // body === JWR.body
            console.log('Sails responded with: ', body);
            this.currentPlay = body.id;
            console.log('with headers: ', JWR.headers);
            console.log('and with status code: ', JWR.statusCode);
            this.setState({
                predictionActive: false,
                status: "INITIAL",
                cardsInHand: []
            })
        })

    }

    changePredictions(event, cardID) {
        const newPrediction = [...this.state.prediction];
        const cardIndex = newPrediction.indexOf(cardID);
        if (cardIndex !== -1 ) {
            newPrediction.splice( cardIndex, 1 ); 
        } else {
            newPrediction.push(cardID)
        }
        this.setState({
            prediction: newPrediction
        })
    }

    submitPrediction() {
        console.log(this.state.prediction)
        console.log(this.currentPlay)
        this.io.socket.post(`/viewer/predict`, {
            prediction: this.state.prediction,
            play: this.currentPlay
        }, () => {
            this.setState({
                status: "INITIAL"
            })
        });
    }

    getCardIds() {
        return axios.get(`${this.BACKEND_URL}/cards-in-hand`)
    }

    getCardDetails(cardIds) {
        return cardDetails.filter((card) => {
            return cardIds.includes(card.cardCode)
        });
    }

    timerEnded() {
        this.setState({
            timerEnded: true,
            status: "COMMITING"
        })
    }

    commitResults() {
        this.getCardIds().then((res) => {
            console.log("played cards :", res.data)
            this.io.socket.post(`/streamer/stop`, {
                hand: res.data,
                play: this.currentPlay
            },
            (body, JWR) => {
                this.setState({
                    status: "INITIAL"
                })
            })

        })

    }

    componentDidMount(){
        if(this.twitch){
            this.twitch.onAuthorized((auth)=>{
                this.Authentication.setToken(auth.token, auth.userId)
                console.log(this.Authentication.state.user_id)
                if(!this.state.finishedLoading){
                    // if the component hasn't finished loading (as in we've not set up after getting a token), let's set it up now.
                    this.io = sailsIOClient(socketIOClient);
                    this.io.sails.url = this.SERVER_ADDRESS;
                    let connectionLink;
                    const userType = this.Authentication.state.role === "broadcaster" ? 'streamer' : 'viewer';
                    if (  userType === "streamer" ) {
                        connectionLink = `/${userType}/join/`;
                        console.log('streamer header : ', auth.channelId)
                        this.io.sails.headers = {
                            "userid": auth.channelId,
                        };
     
                    } else {
                        connectionLink = `/${userType}/join/${auth.channelId}`;
                        this.io.sails.headers = {
                            "userid": auth.userId,
                        };
     
                    }

                    console.log("connecion link : ", connectionLink);
                    this.io.socket.get(connectionLink ,
                    function serverResponded(body, JWR) {
                        // body === JWR.body
                        console.log('Sails responded with: ', body);
                        console.log('with headers: ', JWR.headers);
                        console.log('and with status code: ', JWR.statusCode);
                    })
                    // now we've done the setup for the component, let's set the state to true to force a rerender with the correct data.
                    this.setState(()=>{
                        return {finishedLoading:true}
                    })

                    if (userType === 'viewer') {
                        this.io.socket.on("newPlay", (evt) => {
                            console.log('new play received', evt)
                            this.setState({
                                status: "TIMER",
                                cardsInHand: evt.startHand
                            })
                            this.currentPlay = evt.id;
                        })
                        this.io.socket.on("prediction", (evt) => {
                            console.log('new prediction received', evt)
                        })
                    }

                }
            })

            this.twitch.listen('broadcast',(target,contentType,body)=>{
                this.twitch.rig.log(`New PubSub message!\n${target}\n${contentType}\n${body}`)
                // now that you've got a listener, do something with the result... 

                // do something...

            })

            this.twitch.onVisibilityChanged((isVisible,_c)=>{
                this.visibilityChanged(isVisible)
            })

            this.twitch.onContext((context,delta)=>{
                this.contextUpdate(context,delta)
            })
        }
    }

    componentWillUnmount(){
        if(this.twitch){
            this.twitch.unlisten('broadcast', ()=>console.log('successfully unlistened'))
        }
    }

    renderStreamerView() {
        const { status } = this.state;
        switch (status) {
            case "INITIAL":
                return (
                    <button class="ui button start-predict-button" onClick={() => this.startPred()}>
                        Start Predictions
                    </button>
                );
            case "TIMER":
                return (
                    <div>
                        Predictions ongoing...
                        <Countdown date={Date.now() + 5000} onComplete={() => this.timerEnded()} />
                    </div>
                );
            case "COMMITING":
                return (
                    <button class="ui button start-predict-button"  onClick={() => this.commitResults()}>
                        Commit Results
                    </button>
                );
                break;
        
            default:
                break;
        }
    }

    renderViewerView() {
        const { status } = this.state;
        switch (status) {
            case "INITIAL":
                return (
                    <div class="ui button start-predict-button">
                        Waiting for predictions
                    </div>
                );
            case "TIMER":
                return (
                    <div>
                        {/* <Countdown date={Date.now() + this.TIME_TO_PREDICT} onComplete={() => this.timerEnded()} /> */}
                        <form>
                            {this.getCardDetails(this.state.cardsInHand).map((card) => {
                                return (
                                    <div>
                                        <input
                                            type="checkbox"
                                            className="card-checkbox"
                                            name={card.cardCode}
                                            value={card.cardCode}
                                            onChange={(event) => this.changePredictions(event, card.cardCode)}
                                        /> 
                                        {card.name}
                                    </div>
                                );
                            })}
                        </form>
                        <button class="ui button submit-predict-button"  onClick={() => this.submitPrediction()}>
                            Submit Predictions
                        </button>
                    </div>
                );
            case "COMMITING":
                return (
                    <button class="ui button start-predict-button"  onClick={() => this.commitResults()}>
                        Commit Results
                    </button>
                );
                break;
        
            default:
                break;
        }
    }
    
    render(){
        if(this.state.finishedLoading && this.state.isVisible){
            if (this.Authentication.state.role === "broadcaster") {
                return (
                    <Container className="App">
                        <div className="text-center">Predict my play {status}</div>
                        {this.renderStreamerView()}
                    </Container>
                )
            } else {
                return (
                    <Container className="App">
                        <div className="text-center">Predict my play</div>
                        {this.renderViewerView()}
                    </Container>
                )
            }
        }else{
            return (
                <div className="App">
                </div>
            )
        }

    }
}